package com.teo.promoyt.service;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.teo.promoyt.MainActivity;
import com.teo.promoyt.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;


public class MyGcmListenerService extends FirebaseMessagingService {

    public void onMessageReceived(RemoteMessage message) {
        super.onMessageReceived(message);
        Log.e("PUSH", "From: " + message.getFrom());
        Log.e("PUSH", "data: " + message.getData());

    }

    private static final String channelId = "vk_channel";
    private static final String channelName = "Channel vk";
    private NotificationChannel mChannel;

    private void sendNotification(Context context, int id, String message) {
        Intent intent = new Intent(context, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);


        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= 26 ) {
            this.mChannel = new NotificationChannel(channelId, channelName, NotificationManager.IMPORTANCE_DEFAULT);
            this.mChannel.enableVibration(false);
            this.mChannel.setVibrationPattern(null);
            this.mChannel.enableLights(false);
            this.mChannel.setSound(null, null);
            notificationManager.createNotificationChannel(this.mChannel);
        }
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, channelId);

        builder.setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher));
        builder.setSmallIcon(R.mipmap.ic_launcher);
        builder.setContentTitle(getResources().getString(R.string.app_name));
        builder .setContentText(message);
        builder.setAutoCancel(true);
        builder.setSound(defaultSoundUri);
        builder.setContentIntent(contentIntent);


        startForeground(id, builder.build());
    }

    private String getOrderStatus(int status) {
        switch (status) {
            case 1:
                return "Новый";
            case 2:
                return "В работе";
            case 3:
                return "В доставке";
            case 4:
                return "Выполнен";
            default:
                return "Предварительный";
        }
    }
}
