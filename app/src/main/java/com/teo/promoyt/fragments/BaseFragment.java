package com.teo.promoyt.fragments;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.teo.promoyt.App;
import com.teo.promoyt.activityes.BaseActivity;

public class BaseFragment extends Fragment {



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    public BaseActivity getBaseActivity(){
        return (BaseActivity) getActivity();
    }

    public SharedPreferences getPreferences(){
        return App.getInstance().getPreferences();
    }
}
