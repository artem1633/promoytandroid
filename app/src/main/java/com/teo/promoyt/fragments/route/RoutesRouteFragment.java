package com.teo.promoyt.fragments.route;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.teo.promoyt.App;
import com.teo.promoyt.R;
import com.teo.promoyt.activityes.RouteActivity;
import com.teo.promoyt.adapters.RouteAdapter;
import com.teo.promoyt.fragments.BaseFragment;
import com.teo.promoyt.fragments.route.routes.BaseRouteFragment;
import com.teo.promoyt.model.Route;
import com.teo.promoyt.rest.get.GetRoutes;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RoutesRouteFragment extends BaseRouteFragment implements RouteAdapter.RouteClickListener {

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    private RouteAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_route_list, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        recyclerView.setLayoutManager(new LinearLayoutManager(getBaseActivity()));

        adapter = new RouteAdapter(getBaseActivity());
        adapter.setListener(this);
        recyclerView.setAdapter(adapter);

        getRoutes();
    }

    private void getRoutes(){
        Call<GetRoutes> callRoutes = App.getRetrofitApi().getRoutes(App.getInstance().getUserLogin().getToken());

        callRoutes.enqueue(new Callback<GetRoutes>() {

            @Override
            public void onResponse(Call<GetRoutes> call, Response<GetRoutes> response) {

                GetRoutes getRoutes = response.body();
                if (getRoutes != null && getRoutes.getData() != null){
                    adapter.setList(getRoutes.getData());
                }else {
                    adapter.setList(new ArrayList<Route>());
                }
            }

            @Override
            public void onFailure(Call<GetRoutes> call, Throwable t) {

            }

        });
    }

    @Override
    public void showRoute(Route route, int position) {
        Intent intent = new Intent(getBaseActivity(), RouteActivity.class);
        intent.putExtra("route", route);
        intent.putExtra("position", position);
        startActivityForResult(intent, 100);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        /*int position = data.getIntExtra("position", 0);
        Route route = (Route)data.getSerializableExtra("route");
        adapter.updateRoute(route, position);
        */

        getRoutes();
    }
}
