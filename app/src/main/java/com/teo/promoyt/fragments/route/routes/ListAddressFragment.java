package com.teo.promoyt.fragments.route.routes;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.teo.promoyt.R;
import com.teo.promoyt.activityes.AddressActivity;
import com.teo.promoyt.activityes.RouteActivity;
import com.teo.promoyt.adapters.HouseAdapter;
import com.teo.promoyt.fragments.BaseFragment;
import com.teo.promoyt.model.House;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ListAddressFragment extends BaseRouteFragment implements HouseAdapter.HouseClickListener{

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.item_done)
    TextView textDone;
    @BindView(R.id.item_count)
    TextView textCount;
    @BindView(R.id.item_left)
    TextView textLeft;

    private HouseAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_address_list, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        adapter = new HouseAdapter(getBaseActivity());
        adapter.setListener(this);

        recyclerView.setLayoutManager(new LinearLayoutManager(getBaseActivity()));
        recyclerView.setAdapter(adapter);

        RouteActivity activity = (RouteActivity)getActivity();
        adapter.setList(activity.getHouses());

        updateDone();
    }

    public void updateDone(){
        RouteActivity activity = (RouteActivity)getActivity();

        int count = activity.getHouses().size();
        textCount.setText(String.valueOf(count));

        int done  = 0;
        for (House house : activity.getHouses()){
            done += house.getStatus_api();
        }

        textDone.setText(String.valueOf(done));
        textLeft.setText(String.valueOf(count - done));
    }

    @Override
    public void showRoute(House route, int position) {
        RouteActivity activity = (RouteActivity)getActivity();
        if (activity != null && route.getStatus_api() == 0
                && activity.getRoute().getStatusRoute() != 0
                && activity.getRoute().getStatusRoute() != 1
                && activity.getRoute().getStatusRoute() != 3
                && activity.getRoute().getStatusRoute() != 4
        ){
            Intent intent = new Intent(getBaseActivity(), AddressActivity.class);
            intent.putExtra("house", route);
            intent.putExtra("position", position);
            intent.putExtra("routeId", activity.getRoute().getId());
            startActivityForResult(intent, 200);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK){
            int position = data.getIntExtra("position", 0);
            RouteActivity activity = (RouteActivity)getActivity();
            activity.getHouses().get(position).setStatus_api(1);
            updateDone();
            getBaseActivity().sendBroadcast(new Intent("update_address_data"));
            adapter.notifyItemChanged(position);
        }
    }

}
