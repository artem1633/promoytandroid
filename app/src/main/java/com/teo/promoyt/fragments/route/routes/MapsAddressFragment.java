package com.teo.promoyt.fragments.route.routes;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.teo.promoyt.App;
import com.teo.promoyt.R;
import com.teo.promoyt.activityes.RouteActivity;
import com.teo.promoyt.database.UserLocation;
import com.teo.promoyt.fragments.BaseFragment;
import com.teo.promoyt.model.House;
import com.yandex.mapkit.MapKitFactory;
import com.yandex.mapkit.directions.DirectionsFactory;
import com.yandex.mapkit.geometry.Point;
import com.yandex.mapkit.geometry.Polyline;
import com.yandex.mapkit.layers.ObjectEvent;
import com.yandex.mapkit.map.CameraPosition;
import com.yandex.mapkit.map.MapObject;
import com.yandex.mapkit.map.MapObjectCollection;
import com.yandex.mapkit.map.MapObjectTapListener;
import com.yandex.mapkit.map.PlacemarkMapObject;
import com.yandex.mapkit.map.PolylineMapObject;
import com.yandex.mapkit.mapview.MapView;
import com.yandex.mapkit.user_location.UserLocationLayer;
import com.yandex.mapkit.user_location.UserLocationObjectListener;
import com.yandex.mapkit.user_location.UserLocationView;
import com.yandex.runtime.image.ImageProvider;
import com.yandex.runtime.ui_view.ViewProvider;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MapsAddressFragment extends BaseRouteFragment implements UserLocationObjectListener {

    private final String MAPKIT_API_KEY = "9a088961-4b2a-44a7-88e0-c02f8a3e5d37";

    private final Point ROUTE_START_LOCATION = new Point(55.574257, 37.345048);
    private final Point ROUTE_END_LOCATION = new Point(55.915872, 37.838060);
    private final Point MOSCOW = new Point(
            (ROUTE_START_LOCATION.getLatitude() + ROUTE_END_LOCATION.getLatitude()) / 2,
            (ROUTE_START_LOCATION.getLongitude() + ROUTE_END_LOCATION.getLongitude()) / 2);

    @BindView(R.id.map_view)
    MapView mapView;

    private Map<String, House> mapHouses;
    private UserLocationLayer userLocationLayer;
    private PlacemarkMapObject selectMarker;
    private MapObjectCollection mapObjects;
    private PlacemarkMapObject myPosition;

    private Polyline polyline;

    private BroadcastReceiver receiver;
    private BroadcastReceiver receiverLocation;




    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_address_map, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MapKitFactory.setApiKey(MAPKIT_API_KEY);
        MapKitFactory.initialize(getActivity());
        DirectionsFactory.initialize(getActivity());
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mapView.getMap().setRotateGesturesEnabled(false);
        mapView.getMap().move(new CameraPosition(MOSCOW, 10, 0, 0));

        mapHouses = new HashMap<>();

        userLocationLayer = mapView.getMap().getUserLocationLayer();
        userLocationLayer.setEnabled(true);
        userLocationLayer.setHeadingEnabled(true);
        userLocationLayer.setObjectListener(this);

        addMarkers();

        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                addMarkers();
            }
        };

        receiverLocation = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                addMarkers();
            }
        };

        getBaseActivity().registerReceiver(receiver, new IntentFilter("update_address_data"));
        getBaseActivity().registerReceiver(receiverLocation, new IntentFilter("services_location"));

    }

    private void addPolyline(){
        List<UserLocation> userLocations = App.getInstance().getDatabase().getLocationDao().getAll();
        List<Point> points = new ArrayList<>();
        for (UserLocation userLocation : userLocations){
            points.add(new Point(userLocation.getLatitude(), userLocation.getLongitude()));
        }


        polyline = new Polyline(points);

        PolylineMapObject polylineMapObject = mapObjects.addPolyline(polyline);
        polylineMapObject.setStrokeColor(getResources().getColor(R.color.colorAccent));
        polylineMapObject.setStrokeWidth(10);
    }

    public void addMarkers(){
        try {

            RouteActivity activity = (RouteActivity)getActivity();

            for (House house : activity.getHouses()){
                mapHouses.put(house.getId(), house);
            }

            mapObjects = mapView.getMap().getMapObjects();

            mapObjects.clear();

            addPolyline();

            if (getContext() != null){
                for (final House company : activity.getHouses()) {
                    Point resultLocation = new Point(company.getCoordX(), company.getCoordY());

                    ImageView view = (ImageView) LayoutInflater.from(getBaseActivity()).inflate(R.layout.item_marker, null, false);

                    if (company.getStatus_api() == 1){
                        view.setColorFilter(getResources().getColor(R.color.colorDone));
                    }else {
                        view.setColorFilter(getResources().getColor(R.color.colorLeft));
                    }


                    final ViewProvider viewProvider = new ViewProvider(view);
                    mapObjects.addPlacemark(resultLocation, viewProvider);
                }
            }



        }catch (Throwable e){
            e.printStackTrace();
        }
    }



    @Override
    public void onObjectAdded(@NonNull UserLocationView userLocationView) {
        myPosition = userLocationView.getArrow();

    }

    @Override
    public void onObjectRemoved(@NonNull UserLocationView userLocationView) {

    }

    @Override
    public void onObjectUpdated(@NonNull UserLocationView userLocationView, @NonNull ObjectEvent objectEvent) {
        myPosition = userLocationView.getArrow();
    }

    @Override
    public void onStart() {
        super.onStart();
        mapView.onStart();
        MapKitFactory.getInstance().onStart();
    }

    @Override
    public void onStop() {
        mapView.onStop();
        MapKitFactory.getInstance().onStop();
        super.onStop();
    }

    @Override
    public void onDestroy() {
        getBaseActivity().unregisterReceiver(receiver);
        getBaseActivity().unregisterReceiver(receiverLocation);
        super.onDestroy();
    }
}
