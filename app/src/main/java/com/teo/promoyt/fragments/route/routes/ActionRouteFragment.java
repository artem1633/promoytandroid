package com.teo.promoyt.fragments.route.routes;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.ServiceConnection;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.teo.promoyt.App;
import com.teo.promoyt.BackgroundVideoRecorder;
import com.teo.promoyt.CameraHelper;
import com.teo.promoyt.R;
import com.teo.promoyt.activityes.RouteActivity;
import com.teo.promoyt.location.LocationService;
import com.teo.promoyt.model.Route;
import com.teo.promoyt.rest.PostResult;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


import static android.content.Context.BIND_AUTO_CREATE;
import static com.yandex.runtime.Runtime.getApplicationContext;

public class ActionRouteFragment extends BaseRouteFragment {

    @BindView(R.id.item_apply)
    Button buttonAply;
    @BindView(R.id.item_cancel)
    Button buttonCancel;
    @BindView(R.id.item_start)
    Button buttonStart;
    @BindView(R.id.item_pause)
    Button buttonPause;
    @BindView(R.id.item_stop)
    Button buttonStop;
    @BindView(R.id.camera_preview)
    FrameLayout cameraPreview;


    boolean bound = false;
    ServiceConnection sConn;
    Intent intent;

    private BroadcastReceiver receiver;

    private BackgroundVideoRecorder videoRecorder;
    private Route route;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_action_route, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        final RouteActivity activity = (RouteActivity)getActivity();

        route = activity.getRoute();


        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                initButtons();
            }
        };

        getActivity().registerReceiver(receiver, new IntentFilter("services_location_init"));


        /*
activity.setRouteStatus(0, new RouteActivity.StatusChangeListener() {
                    @Override
                    public void onSuccess(int status) {
                        initButtons();
                    }
                });
        */


        intent = new Intent(getActivity(), BackgroundVideoRecorder.class);
        intent.putExtra("route_id", route.getId());

        sConn = new ServiceConnection() {
            public void onServiceConnected(ComponentName name, IBinder binder) {
                Log.e("tr", "MainActivity onServiceConnected");
                bound = true;
                videoRecorder = ((BackgroundVideoRecorder.MyBinder) binder).getService();

                initButtons();

            }

            public void onServiceDisconnected(ComponentName name) {
                Log.e("tr", "MainActivity onServiceDisconnected");
                bound = false;
            }
        };

        getActivity().bindService(intent, sConn, BIND_AUTO_CREATE);


    }

    private void initButtons(){

        int status = route.getStatusRoute();

        buttonPause.setText("Пауза");

        switch (status){
            case 0:

                buttonAply.setEnabled(true);
                buttonCancel.setEnabled(false);

                buttonStart.setEnabled(false);
                buttonStop.setEnabled(false);
                buttonPause.setEnabled(false);
                break;

            case 2:
                buttonCancel.setEnabled(true);
                buttonAply.setEnabled(false);

                buttonStart.setEnabled(true);
                buttonStop.setEnabled(false);
                buttonPause.setEnabled(false);
                break;

            case 3:
                buttonCancel.setEnabled(false);
                buttonAply.setEnabled(false);

                buttonStart.setEnabled(false);
                buttonStop.setEnabled(false);
                buttonPause.setEnabled(false);
                break;

            case 6:
                buttonStart.setEnabled(false);
                buttonStop.setEnabled(true);
                buttonPause.setEnabled(true);

                buttonCancel.setEnabled(false);
                buttonAply.setEnabled(false);
                break;

            case 5:
                buttonPause.setText("Возобновить");

                buttonStart.setEnabled(false);
                buttonStop.setEnabled(true);
                buttonPause.setEnabled(true);

                buttonCancel.setEnabled(false);
                buttonAply.setEnabled(false);
                break;

            case 4:
                buttonStart.setEnabled(false);
                buttonStop.setEnabled(false);
                buttonPause.setEnabled(false);

                buttonCancel.setEnabled(false);
                buttonAply.setEnabled(false);
                break;
        }

    }

    @OnClick(R.id.item_apply)
    public void applyRoute(){
        final RouteActivity activity = (RouteActivity)getActivity();

        new AlertDialog.Builder(activity)
                .setTitle("Принять заказ")
                .setMessage(route.getName())
                .setPositiveButton("Принять", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Call<PostResult> call = App.getRetrofitApi().applyPoutes(App.getInstance().getUserLogin().getToken(), route.getId());
                        call.enqueue(new Callback<PostResult>() {
                            @Override
                            public void onResponse(Call<PostResult> call, Response<PostResult> response) {
                                PostResult postResult = response.body();
                                if (postResult != null){
                                    if (postResult.getErrors() == null){
                                        route.setStatusRoute(2);
                                        initButtons();
                                    }
                                }
                            }

                            @Override
                            public void onFailure(Call<PostResult> call, Throwable t) {

                            }
                        });
                    }
                })
                .setNegativeButton("отмена", null)
                .show();
    }

    @OnClick(R.id.item_cancel)
    public void cancelRoute(){

        final RouteActivity activity = (RouteActivity)getActivity();

        View view = LayoutInflater.from(activity).inflate(R.layout.layout_add_playlist, null, false);
        final EditText editText = view.findViewById(R.id.item_edit);

        new AlertDialog.Builder(activity)
                .setView(view)
                .setTitle("Отказ от заказа")
                .setMessage(route.getName())
                .setPositiveButton("Отказаться", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String name = editText.getText().toString();
                        if (!TextUtils.isEmpty(name)){
                            Call<PostResult> call = App.getRetrofitApi().cancelPoutes(App.getInstance().getUserLogin().getToken(), route.getId(), name);
                            call.enqueue(new Callback<PostResult>() {
                                @Override
                                public void onResponse(Call<PostResult> call, Response<PostResult> response) {
                                    PostResult postResult = response.body();
                                    if (postResult != null){
                                        if (postResult.getErrors() == null){
                                            route.setStatusRoute(3);
                                            initButtons();
                                        }
                                    }
                                }

                                @Override
                                public void onFailure(Call<PostResult> call, Throwable t) {

                                }
                            });
                        }
                    }
                })
                .setNegativeButton("отмена", null)
                .show();
    }

    @OnClick(R.id.item_start)
    public void startCam(){
        final RouteActivity activity = (RouteActivity)getActivity();

        activity.setRouteStatus(6, new RouteActivity.StatusChangeListener() {
            @Override
            public void onSuccess(int status) {
                switchRouteStatus(status);
            }
        });
    }


    @OnClick(R.id.item_stop)
    public void stopCam(){
        final RouteActivity activity = (RouteActivity)getActivity();

        activity.setRouteStatus(4, new RouteActivity.StatusChangeListener() {
            @Override
            public void onSuccess(int status) {
                switchRouteStatus(status);
            }
        });
    }

    /*
    const ROUTE_NEW = 0; //Новый
    const ROUTE_PLAN = 1; //Планируется
    const ROUTE_WORK = 2; //В работе
    const ROUTE_CANCEL = 3; //Отказался
    const ROUTE_DONE = 4; //Завершен
    const ROUTE_PAUSE = 5; //На паузе
    const ROUTE_START = 6; //Начат
     */

    @OnClick(R.id.item_pause)
    public void pauseCam(){
        final RouteActivity activity = (RouteActivity)getActivity();


        if (route.getStatusRoute() == 6) {
            activity.setRouteStatus(5, new RouteActivity.StatusChangeListener() {
                @Override
                public void onSuccess(int status) {
                    switchRouteStatus(status);
                }
            });
        }else {
            activity.setRouteStatus(6, new RouteActivity.StatusChangeListener() {
                @Override
                public void onSuccess(int status) {
                    switchRouteStatus(status);
                }
            });
        }
    }



    private void switchRouteStatus(int status){
        switch (status){

            case 6:
                start();
                break;
            case 5:
                pause();
                break;
            case 4:
                stop();
                break;
        }

        initButtons();
    }

    private void start(){



        if (videoRecorder != null){
            videoRecorder.setHeight(1);
            videoRecorder.start(route.getId());
        }
    }

    private void stop(){

        //cameraHelper.stop();
        if (videoRecorder != null){
            videoRecorder.stop();
        }
    }

    private void pause(){
        if (route.getStatusRoute() == 6) {
            start();
        }else {
            stop();
        }
    }


    @Override
    public void onDestroy() {

        if (videoRecorder != null){
            videoRecorder.stop();
        }

        getActivity().unbindService(sConn);
        getActivity().unregisterReceiver(receiver);

        //cameraHelper.stop();
        super.onDestroy();
    }

    @Override
    public void onPause() {
        super.onPause();

    }




}
