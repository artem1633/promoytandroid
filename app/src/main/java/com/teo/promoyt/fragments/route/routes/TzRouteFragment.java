package com.teo.promoyt.fragments.route.routes;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.teo.promoyt.App;
import com.teo.promoyt.R;
import com.teo.promoyt.activityes.RouteActivity;
import com.teo.promoyt.rest.get.GetTz;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TzRouteFragment extends BaseRouteFragment {

    @BindView(R.id.item_text)
    TextView textView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tz, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        RouteActivity activity = (RouteActivity)getActivity();
        Call<GetTz> call = App.getRetrofitApi().getTz(App.getInstance().getUserLogin().getToken(), activity.getRoute().getId());
        call.enqueue(new Callback<GetTz>() {
            @Override
            public void onResponse(Call<GetTz> call, Response<GetTz> response) {
                if (response != null){
                    GetTz getTz = response.body();
                    if (getTz.getData() != null){
                        textView.setText(Html.fromHtml(getTz.getData()));
                    }
                }
            }

            @Override
            public void onFailure(Call<GetTz> call, Throwable t) {

            }
        });

    }


}
