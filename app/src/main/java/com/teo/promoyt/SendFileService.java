package com.teo.promoyt;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.text.TextUtils;
import android.util.Log;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.teo.promoyt.classes.Utils;
import com.teo.promoyt.rest.CountingRequestBody;
import com.teo.promoyt.rest.PostResult;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class SendFileService extends Service {


    public static void startAction(Context context, String route_id, String token) {
        Intent intent = new Intent(context, SendFileService.class);
        intent.putExtra("route_id", route_id);
        intent.putExtra("token", token);
        if (Utils.isOreo()){
            context.startForegroundService(intent);
        }else {
            context.startService(intent);
        }

    }

    public static final String CHANNEL_ID = "promout_file_sender";
    private long mNotificationPostTime = 0;

    private List<String> routes;

    private NotificationManagerCompat mNotificationManager;

    @Override
    public void onCreate() {
        super.onCreate();

        routes = new ArrayList<>();
        mNotificationManager = NotificationManagerCompat.from(this);
        createNotificationChannel();
    }

    private void createNotificationChannel() {

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            CharSequence name = "Promout";
            int importance = NotificationManager.IMPORTANCE_LOW;
            NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
            manager.createNotificationChannel(mChannel);
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        Log.e("tr", "start");
        String token = intent.getStringExtra("token");
        String route = intent.getStringExtra("route_id");
        if (!routes.contains(route)){
            App.getInstance().setSend(true);
            routes.add(route);
            startUpload(token, route);
        }

        return Service.START_REDELIVER_INTENT;
    }

    private void startUpload(String token, String route){
        File folder = Utils.getBaseFolder(this, route);
        File[] list = folder.listFiles();
        uploadFile(list, 0, token, route);
    }


    private boolean send = false;
    private int failcount;

    public void uploadFile(final File[] files, final int position, final String token, final String route){


            try {
                if (files != null && files.length > 0){

                    send = true;
                    final File file = files[position];
                    final NotificationCompat.Builder builder = buildNotification();
                    final int filePosition = position + 1;
                    updateNotif(builder, "Отправка" + filePosition + "/" + files.length, Integer.parseInt(route));

                    final RequestBody requestFile = RequestBody.create(MediaType.parse("video/*"), file);

                    // MultipartBody.Part is used to send also the actual file name
                    String name = file.getName();
                    String date = name.substring(0, name.indexOf(" part_"));
                    Log.e("tr", "name " + name + " date " + date);
                    RequestBody requestBody = new CountingRequestBody(requestFile, new CountingRequestBody.Listener() {
                        @Override
                        public void onRequestProgress(long bytesWritten, long contentLength) {
                            Log.e("tr", "bw " + bytesWritten + " cl " + contentLength);
                            builder.setProgress((int)contentLength, (int)bytesWritten, false);
                            updateNotif(builder,"Отправка" + filePosition + "/" + files.length, Integer.parseInt(route));
                        }
                    });

                    MultipartBody.Part body = MultipartBody.Part.createFormData("file", name, requestBody);

                    int uploads_files = position == files.length - 1 ? 1 : 0;

                    Call<PostResult> call;
                    if (uploads_files == 1) {
                        call = App.getRetrofitApi().upload(token, date, route, file.length(), body, uploads_files);
                    }else {
                        call = App.getRetrofitApi().upload(token, date, route, file.length(), body);
                    }

                    call.enqueue(new Callback<PostResult>() {
                        @Override
                        public void onResponse(Call<PostResult> call, Response<PostResult> response) {

                            if (response.body() == null){
                                builder.setProgress(0, 0, false);
                                updateNotif(builder, "Ошибка отправки, проверьте соединение!",  Integer.parseInt(route));
                                stopSelf();
                            }else if (response.body().getErrors() == null){
                                Log.e("tr", "position " + position);

                                if (file.delete()){
                                    if (position < files.length - 1){
                                        uploadFile(files, filePosition, token, route);

                                    }else {
                                        builder.setProgress(0, 0, false);
                                        updateNotif(builder, "Отправка завершена", Integer.parseInt(route));
                                        send = false;
                                        stopSelf();
                                    }
                                }

                            }else {
                                Log.e("tr", "Error " + response.body().getErrors());
                                routes.remove(route);
                                mNotificationManager.cancel(Integer.parseInt(route));

                                final NotificationCompat.Builder builder = buildNotification();
                                builder.setProgress(0, 0, false);
                                updateNotif(builder, response.body().getErrors() + " - "+ file.getName(), -1);
                            }
                        }

                        @Override
                        public void onFailure(Call<PostResult> call, Throwable t) {
                            t.printStackTrace();
                            Log.e("tr", "failure " + t.getMessage());
                            mNotificationManager.cancel(Integer.parseInt(route));
                            failcount++;

                            if (failcount < 3 && position < files.length - 1){
                                uploadFile(files, position, token, route);
                            }else {
                                final NotificationCompat.Builder builder = buildNotification();
                                builder.setProgress(0, 0, false);
                                updateNotif(builder, "Ошибка отправки " + position + "/" + files.length + " " +t.getMessage() + " - "+ file.getName(), -1);
                                stopSelf();
                            }
                        }

                    });
                }

            }catch (Throwable e){
                e.printStackTrace();
            }
    }

    private void updateNotif(NotificationCompat.Builder builder, String text, int id){
        builder.setContentText(text);
        if (Utils.isOreo()){
            Log.e("tr", "update notif");
            startForeground(id, builder.build());
        }else {
            mNotificationManager.notify(id, builder.build());
        }

    }

    private NotificationCompat.Builder buildNotification() {

        if (mNotificationPostTime == 0) {
            mNotificationPostTime = System.currentTimeMillis();
        }

        android.support.v4.app.NotificationCompat.Builder builder = new android.support.v4.app.NotificationCompat.Builder(this, CHANNEL_ID)
                .setSmallIcon(Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP ? R.drawable.baseline_email_white_24 : R.drawable.ic_baseline_email_24px)

                .setContentTitle("Отправка файлов")
                .setProgress(100, 0, false)
                .setWhen(mNotificationPostTime);

        if (Utils.isJellyBeanMR1()) {
            builder.setShowWhen(false);
        }

        if (Utils.isOreo()) {
            builder.setColorized(true);
        }

        return builder;
    }

    @Override
    public void onDestroy() {
        App.getInstance().setSend(false);
        //mNotificationManager.cancelAll();
        if (send){
            final NotificationCompat.Builder builder = buildNotification();
            builder.setProgress(0, 0, false);
            updateNotif(builder, "Сервис отправки остановлен системой", 10);
        }
        super.onDestroy();
    }


    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
