package com.teo.promoyt.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.teo.promoyt.R;
import com.teo.promoyt.model.Route;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RouteAdapter extends RecyclerView.Adapter<RouteAdapter.ViewHolder>{

    public RouteAdapter(Context context) {
        this.context = context;
        list = new ArrayList<>();
    }

    private Context context;

    public void setListener(RouteClickListener listener) {
        this.listener = listener;
    }

    private RouteClickListener listener;

    public void setList(List<Route> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    public void updateRoute(Route route, int position){
        list.set(position, route);
        notifyItemChanged(position);
    }

    private List<Route> list;


    public interface RouteClickListener{
        void showRoute(Route route, int position);
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_route, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int i) {
        final Route item = list.get(i);

        holder.textData.setText(item.getPlaneStartDate());
        holder.textAddress.setText(item.getZone());
        holder.textEdition.setText(item.getCirculation());
        holder.textPay.setText(item.getPayment());
        holder.textProject.setText(item.getProject());
        holder.textType.setText(item.getPlacement());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.showRoute(item, i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.item_data)
        TextView textData;
        @BindView(R.id.item_project)
        TextView textProject;
        @BindView(R.id.item_type)
        TextView textType;
        @BindView(R.id.item_edition)
        TextView textEdition;
        @BindView(R.id.item_pay)
        TextView textPay;
        @BindView(R.id.item_address)
        TextView textAddress;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
