package com.teo.promoyt.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.teo.promoyt.R;
import com.teo.promoyt.model.Entrance;
import com.teo.promoyt.model.House;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EntranceAdapter extends RecyclerView.Adapter<EntranceAdapter.ViewHolder>{

    public EntranceAdapter(Context context) {
        this.context = context;
        list = new ArrayList<>();
    }

    private Context context;

    public void setList(List<Entrance> list) {
        this.list = list;
        notifyDataSetChanged();
    }


    public List<Entrance> getList() {
        return list;
    }

    private List<Entrance> list;


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_entrance, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int i) {
        final Entrance item = list.get(i);

        item.setNumber(i + 1);
        holder.textEntrance.setText(item.getNumber() + "под");

        holder.checkPorter.setChecked(item.isPorter());
        holder.checkPorter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                item.setPorter(holder.checkPorter.isChecked());
            }
        });

        if (item.getFloor() > 0){
            holder.editFloor.setText(String.valueOf(item.getFloor()));
        }else {
            holder.editFloor.setText("");
        }

        if (item.getPorch() > 0){
            holder.editPorch.setText(String.valueOf(item.getPorch()));
        }else {
            holder.editPorch.setText("");
        }

        if (item.getN() > 0){
            holder.editN.setText(String.valueOf(item.getN()));
        }else {
            holder.editN.setText("");
        }

        holder.editFloor.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String text = s.toString();
                if (!TextUtils.isEmpty(text)){
                    int t = Integer.parseInt(text);
                    item.setFloor(t);
                }
            }
        });

        if (item.getApartment() > 0){
            holder.editApartment.setText(String.valueOf(item.getApartment()));
        }else {
            holder.editApartment.setText("");
        }

        holder.editApartment.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String text = s.toString();
                if (!TextUtils.isEmpty(text)){
                    int t = Integer.parseInt(text);
                    item.setApartment(t);
                }
            }
        });


        holder.imageRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                list.remove(i);
                notifyDataSetChanged();
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.edit_floor)
        EditText editFloor;
        @BindView(R.id.edit_porch)
        EditText editPorch;
        @BindView(R.id.edit_n)
        EditText editN;
        @BindView(R.id.edit_apartment)
        EditText editApartment;

        @BindView(R.id.check_porter)
        CheckBox checkPorter;
        @BindView(R.id.item_entrance)
        TextView textEntrance;
        @BindView(R.id.item_remove)
        ImageView imageRemove;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
