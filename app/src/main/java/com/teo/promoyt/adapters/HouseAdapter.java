package com.teo.promoyt.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.teo.promoyt.R;
import com.teo.promoyt.model.House;
import com.teo.promoyt.model.Route;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HouseAdapter extends RecyclerView.Adapter<HouseAdapter.ViewHolder>{

    public HouseAdapter(Context context) {
        this.context = context;
        list = new ArrayList<>();
    }

    private Context context;

    public void setListener(HouseClickListener listener) {
        this.listener = listener;
    }

    private HouseClickListener listener;


    public void setList(List<House> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    public void updateRoute(House route, int position){
        list.set(position, route);
        notifyItemChanged(position);
    }

    private List<House> list;

    public interface HouseClickListener{
        void showRoute(House route, int position);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_house, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int i) {
        final House item = list.get(i);

        if (TextUtils.isEmpty(item.getHousing())){
            holder.textTitle.setText(item.getStreet() + " " + item.getHouse());
        }else {
            holder.textTitle.setText(item.getStreet() + " " + item.getHouse() + ", корпус " + item.getHousing());
        }


        if (item.getStatus_api() == 1){
            holder.indicatorView.setColorFilter(context.getResources().getColor(R.color.colorDone));
        }else {
            holder.indicatorView.setColorFilter(context.getResources().getColor(R.color.colorLeft));
        }


        holder.itemView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                listener.showRoute(item, i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.item_title)
        TextView textTitle;
        @BindView(R.id.item_icon)
        ImageView indicatorView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
