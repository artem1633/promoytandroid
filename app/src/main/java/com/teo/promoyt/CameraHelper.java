package com.teo.promoyt;

import android.content.Context;
import android.content.IntentSender;
import android.hardware.Camera;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.teo.promoyt.activityes.RouteActivity;
import com.teo.promoyt.classes.Utils;
import com.teo.promoyt.location.LocationService;

import java.io.File;
import java.io.IOException;

import static com.yandex.runtime.Runtime.getApplicationContext;

public class CameraHelper {

    private Camera mCamera;


    public SurfaceView getSurfaceView() {
        return surfaceView;
    }

    private SurfaceView surfaceView;
    private MediaRecorder mediaRecorder;


    private String id;
    private File folder;

    public CameraHelper(Context mCamera, String id) {
        this.context = mCamera;
        this.id = id;
        folder = Utils.getBaseFolder(mCamera, id);
    }

    private Context context;

    public interface OnStartListener{
        void onStart(Camera.Size size, SurfaceView surfaceView);
    }

    private OnStartListener listener;

    public void start(OnStartListener listener){
        surfaceView = new SurfaceView(context);
        this.listener = listener;
        mCamera = getCameraInstance();
        Camera.Parameters params = mCamera.getParameters();

        params.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);

        mCamera.setParameters(params);

        Camera.Size size = mCamera.getParameters().getPreviewSize();

        listener.onStart(size, surfaceView);

        surfaceView.getHolder().addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder holder) {
                if (prepareVideoRecorder()) {
                    mediaRecorder.start();
                } else {
                    releaseMediaRecorder();

                }
            }

            @Override
            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

            }

            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {

            }
        });

        //folder = Utils.getBaseFolder(getBaseActivity(), route.getId());

        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);

        final SettingsClient client = LocationServices.getSettingsClient(context);
        Task<LocationSettingsResponse> task = client.checkLocationSettings(builder.build());

        task.addOnSuccessListener(new OnSuccessListener<LocationSettingsResponse>() {
            @Override
            public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                LocationService.getInstance(context).init(getApplicationContext(), id, App.getInstance().getUserLogin().getToken());

            }
        });

        task.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                int statusCode = ((ApiException) e).getStatusCode();
                switch (statusCode) {
                    case CommonStatusCodes.RESOLUTION_REQUIRED:

                        /*try {
                            ResolvableApiException resolvable = (ResolvableApiException) e;
                            //resolvable.startResolutionForResult(context, 500);

                        } catch (IntentSender.SendIntentException sendEx) {

                        }*/
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:

                        break;
                }
            }
        });
    }

    private void releaseMediaRecorder(){
        if (mediaRecorder != null) {
            mediaRecorder.reset();
            mediaRecorder.release();
            mediaRecorder = null;

            Log.e("tr", "mediaRecorder release");
        }
    }

    private void releaseCamera(){
        if (mCamera != null){
            mCamera.release();
            mCamera = null;
            Log.e("tr", "camera release");
        }

    }

    public static Camera getCameraInstance(){
        Camera c = null;
        try {
            c = Camera.open();
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return c;
    }

    private boolean prepareVideoRecorder(){
        mediaRecorder = new MediaRecorder();
        mCamera.setDisplayOrientation(90);
        mediaRecorder.setOrientationHint(90);
        mCamera.unlock();
        mediaRecorder.setCamera(mCamera);
        //mediaRecorder.setAudioSource(MediaRecorder.AudioSource.CAMCORDER);
        mediaRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);
        mediaRecorder.setMaxDuration(1000 * 60 * 5);
        mediaRecorder.setProfile(CamcorderProfile.get(CamcorderProfile.QUALITY_LOW));
        mediaRecorder.setOutputFile(Utils.getCamFile(folder).toString());
        mediaRecorder.setPreviewDisplay(surfaceView.getHolder().getSurface());
        mediaRecorder.setOnInfoListener(new MediaRecorder.OnInfoListener() {
            @Override
            public void onInfo(MediaRecorder mr, int what, int extra) {
                if (what == MediaRecorder.MEDIA_RECORDER_INFO_MAX_DURATION_REACHED) {
                    stop();
                    start(listener);
                }
            }
        });

        try {
            mediaRecorder.prepare();
        } catch (IllegalStateException e) {
            Log.d("tr", "IllegalStateException preparing MediaRecorder: " + e.getMessage());
            releaseMediaRecorder();
            return false;
        } catch (IOException e) {
            Log.d("tr", "IOException preparing MediaRecorder: " + e.getMessage());
            releaseMediaRecorder();
            return false;
        }
        return true;

    }

    public void stop(){
        LocationService.getInstance(context).onDestroy();
        if (mediaRecorder != null) {
           mediaRecorder.stop();
           Log.e("tr", "mediaRecorder stop");
        }
        releaseMediaRecorder();
        releaseCamera();

    }
}
