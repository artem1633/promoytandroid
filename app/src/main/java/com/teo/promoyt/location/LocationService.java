package com.teo.promoyt.location;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Handler;
import android.util.Log;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.teo.promoyt.App;
import com.teo.promoyt.database.UserLocation;
import com.teo.promoyt.rest.PostResult;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by trablone on 7/2/17.
 */

public class LocationService {

    private static LocationService INSTANCE = null;
    private LocationCallback mLocationCallback;
    private LocationRequest mLocationRequest;
    private FusedLocationProviderClient mFusedLocationClient;
    private Location location;
    private String route_id;
    private String token;

    private Handler handler;
    private int timecode;

    private Context context;

    public LocationService(Context context) {
        INSTANCE = this;
        this.context = context;
        handler = new Handler();
    }

    public static LocationService getInstance(Context context) {
        if (INSTANCE == null) {
            INSTANCE = new LocationService(context);
        }

        return INSTANCE;
    }

    public Runnable runnable = new Runnable() {
        @Override
        public void run() {
            timecode += 1;
            handler.postDelayed(this, 1000);
        }
    };

    @SuppressLint("MissingPermission")
    public void init(final Context context, String route_id, String token) {
        this.route_id = route_id;
        this.token = token;
        try {
            mFusedLocationClient = LocationServices.getFusedLocationProviderClient(context);
            mLocationCallback = new LocationCallback() {
                @Override
                public void onLocationResult(LocationResult locationResult) {
                    if (locationResult != null) {
                        location = locationResult.getLastLocation();
                        if (location != null) {
                            sendLocation(location);

                        }
                    }
                }
            };

            mLocationRequest = new LocationRequest();
            mLocationRequest.setInterval(1000* 10);
            mLocationRequest.setFastestInterval(1000 * 10);
            mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            if (mFusedLocationClient != null)
                mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, null);

            sendInit();

            handler.post(runnable);

        } catch (Throwable e) {
            Log.e("tr", "Service: " + e.getMessage());
        }
    }

    private void sendInit(){
        Intent intent = new Intent("services_location_init");
        context.sendBroadcast(intent);
    }

    public void sendLocation(Location location) {

        UserLocation userLocation = new UserLocation();
        userLocation.setDate(System.currentTimeMillis());
        userLocation.setLatitude(location.getLatitude());
        userLocation.setLongitude(location.getLongitude());
        userLocation.setRoute(route_id);

        App.getInstance().getDatabase().getLocationDao().insert(userLocation);

        Call<PostResult> call = App.getRetrofitApi().setGeo(timecode, token, route_id, location.getLatitude(), location.getLongitude());
        call.enqueue(new Callback<PostResult>() {
            @Override
            public void onResponse(Call<PostResult> call, Response<PostResult> response) {

            }

            @Override
            public void onFailure(Call<PostResult> call, Throwable t) {

            }
        });

        Intent intent = new Intent("services_location");
        intent.putExtra("location", location);
        context.sendBroadcast(intent);



    }
    public Location getLocation() {
        return location;
    }


    public void onDestroy() {

        handler.removeCallbacks(runnable);
        timecode = 0;

        if (mFusedLocationClient != null) {
            mFusedLocationClient.removeLocationUpdates(mLocationCallback);
        }

        sendInit();
        Log.e("tr", "UserLocation onDestroy");

    }


}
