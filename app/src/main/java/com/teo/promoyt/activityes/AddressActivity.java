package com.teo.promoyt.activityes;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.teo.promoyt.App;
import com.teo.promoyt.R;
import com.teo.promoyt.adapters.EntranceAdapter;
import com.teo.promoyt.model.Entrance;
import com.teo.promoyt.model.House;
import com.teo.promoyt.rest.PostResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddressActivity extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.item_apartment)
    TextView textApartment;
    @BindView(R.id.item_entrance)
    TextView textEntrance;
    @BindView(R.id.item_floor)
    TextView textFloor;
    @BindView(R.id.item_porter)
    TextView textPorter;

    @BindView(R.id.recycler_view)
    LinearLayout recyclerView;
    @BindView(R.id.item_comment)
    EditText editComment;

    private House house;
    private int position;

    private String routeId;

    private List<Entrance> list;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address);

        ButterKnife.bind(this);

        house = (House) getIntent().getSerializableExtra("house");
        position = getIntent().getIntExtra("position", 0);
        routeId = getIntent().getStringExtra("routeId");

        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);

        actionBar.setTitle(house.getStreet() + " " + house.getHouse());

        textApartment.setText(String.valueOf(house.getApartament()));
        textFloor.setText(String.valueOf(house.getFloor()));
        textEntrance.setText(String.valueOf(house.getEntrance()));
        textPorter.setText(String.valueOf(house.getPorter()));




        list = new ArrayList<>();
        for (int i = 0; i < house.getEntrance(); i++){
            list.add(new Entrance());
        }

        init();

    }

    private void init(){
        recyclerView.removeAllViews();
        for (int i = 0; i < list.size(); i++){
            addItem(list.get(i), i);
        }
    }

    private void addItem(final Entrance item, final int position){
        final View view = LayoutInflater.from(this).inflate(R.layout.item_entrance, recyclerView, false);
        final ViewHolder holder = new ViewHolder(view);

        item.setNumber(position + 1);
        holder.textEntrance.setText(item.getNumber() + "под");

        holder.checkPorter.setChecked(item.isPorter());
        holder.checkPorter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                item.setPorter(holder.checkPorter.isChecked());
            }
        });

        if (item.getFloor() > 0){
            holder.editFloor.setText(String.valueOf(item.getFloor()));
        }else {
            holder.editFloor.setText("");
        }

        if (item.getPorch() > 0){
            holder.editPorch.setText(String.valueOf(item.getPorch()));
        }else {
            holder.editPorch.setText("");
        }

        holder.editPorch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String text = s.toString();
                if (!TextUtils.isEmpty(text)){
                    int t = Integer.parseInt(text);
                    item.setPorch(t);
                }
            }
        });
        if (item.getN() > 0){
            holder.editN.setText(String.valueOf(item.getN()));
        }else {
            holder.editN.setText("");
        }
        holder.editN.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String text = s.toString();
                if (!TextUtils.isEmpty(text)){
                    int t = Integer.parseInt(text);
                    item.setN(t);
                }
            }
        });

        holder.editFloor.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String text = s.toString();
                if (!TextUtils.isEmpty(text)){
                    int t = Integer.parseInt(text);
                    item.setFloor(t);
                }
            }
        });

        if (item.getApartment() > 0){
            holder.editApartment.setText(String.valueOf(item.getApartment()));
        }else {
            holder.editApartment.setText("");
        }

        holder.editApartment.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String text = s.toString();
                if (!TextUtils.isEmpty(text)){
                    int t = Integer.parseInt(text);
                    item.setApartment(t);
                }
            }
        });


        holder.imageRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                list.remove(position);
                init();
            }
        });

        recyclerView.addView(view);
    }

    public class ViewHolder {

        @BindView(R.id.edit_floor)
        EditText editFloor;
        @BindView(R.id.edit_apartment)
        EditText editApartment;
        @BindView(R.id.edit_porch)
        EditText editPorch;
        @BindView(R.id.edit_n)
        EditText editN;
        @BindView(R.id.check_porter)
        CheckBox checkPorter;
        @BindView(R.id.item_entrance)
        TextView textEntrance;
        @BindView(R.id.item_remove)
        ImageView imageRemove;

        public ViewHolder(@NonNull View itemView) {
            ButterKnife.bind(this, itemView);
        }
    }

    @OnClick(R.id.item_save)
    public void save(){


        JSONObject root = new JSONObject();

        JSONArray array = new JSONArray();
        for (int i = 0; i < list.size(); i++){
            Entrance entrance = list.get(i);

            JSONObject object = new JSONObject();
            try {
                /*
                if (entrance.getFloor() < 1){
                    Toast.makeText(this, entrance.getNumber() + "под. Укажите этажи", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (entrance.getApartment() < 1){
                    Toast.makeText(this, entrance.getNumber() + "под. Укажите квартиры", Toast.LENGTH_SHORT).show();
                    return;
                }
                */
                object.put("apartment_count", entrance.getApartment());
                object.put("floor_count", entrance.getFloor());
                object.put("number", entrance.getNumber());
                object.put("porch", entrance.getPorch());
                object.put("n", entrance.getN());
                object.put("porter", entrance.isPorter() ? 1 : 0);
                array.put(object);


            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        try {
            root.put("token", App.getInstance().getUserLogin().getToken());
            root.put("route", routeId);
            root.put("address", house.getId());
            root.put("comment", editComment.getText().toString());
            root.put("entrances", array);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String listArray = root.toString();

        Call<PostResult> call = App.getRetrofitApi().setAddress(listArray);
        call.enqueue(new Callback<PostResult>() {
            @Override
            public void onResponse(Call<PostResult> call, Response<PostResult> response) {
                if (response.body() != null){
                    PostResult result = response.body();

                    if (result.getErrors() == null){
                        Intent intent = new Intent();
                        intent.putExtra("position", position);
                        setResult(RESULT_OK, intent);
                        finish();
                    }else {
                        Toast.makeText(AddressActivity.this, "Ошибка " + result.getErrors(), Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<PostResult> call, Throwable t) {

            }
        });

    }

    @OnClick(R.id.item_add)
    public void add(){
        list.add(new Entrance());
        addItem(list.get(list.size() - 1), list.size() - 1);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
