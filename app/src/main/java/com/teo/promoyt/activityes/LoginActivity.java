package com.teo.promoyt.activityes;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.teo.promoyt.App;
import com.teo.promoyt.MainActivity;
import com.teo.promoyt.R;
import com.teo.promoyt.database.UserLogin;
import com.teo.promoyt.rest.get.GetLogin;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends BaseActivity {

    @BindView(R.id.item_login)
    EditText editLogin;
    @BindView(R.id.item_pass)
    EditText editPass;
    @BindView(R.id.item_send)
    Button buttonSend;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

    }

    @OnClick(R.id.item_send)
    public void login(){
        String login = editLogin.getText().toString();
        String pass = editPass.getText().toString();
        String androidId = "3211";//Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Call<GetLogin> call = App.getRetrofitApi().login(login, pass, androidId, refreshedToken);
        call.enqueue(new Callback<GetLogin>() {
            @Override
            public void onResponse(Call<GetLogin> call, Response<GetLogin> response) {
                GetLogin getLogin = response.body();
                if (TextUtils.isEmpty(getLogin.getErrors())){
                    Log.e("tr", "resp " + getLogin.getToken());
                    UserLogin userLogin = new UserLogin();
                    userLogin.setToken(getLogin.getToken());

                    App.getInstance().getDatabase().getHistoryDao().insert(userLogin);
                    startActivity(new Intent(LoginActivity.this, MainActivity.class));
                    finish();

                }else {
                    Toast.makeText(LoginActivity.this, "Error " + getLogin.getErrors(), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<GetLogin> call, Throwable t) {

            }
        });
    }
}
