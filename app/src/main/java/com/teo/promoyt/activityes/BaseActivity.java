package com.teo.promoyt.activityes;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.teo.promoyt.App;
import com.teo.promoyt.database.UserLogin;

import java.util.List;

public class BaseActivity extends AppCompatActivity {

    public Handler handler = new Handler();



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



    }
    public SharedPreferences getPreferences(){
        return App.getInstance().getPreferences();
    }
}
