package com.teo.promoyt.activityes;


import android.Manifest;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.teo.promoyt.App;
import com.teo.promoyt.BackgroundVideoRecorder;
import com.teo.promoyt.FileHelper;
import com.teo.promoyt.R;
import com.teo.promoyt.SendFileService;
import com.teo.promoyt.classes.Utils;
import com.teo.promoyt.fragments.route.routes.RouteTabsFragment;
import com.teo.promoyt.model.House;
import com.teo.promoyt.model.Route;
import com.teo.promoyt.rest.PostResult;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RouteActivity extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    public Route getRoute() {
        return route;
    }

    private Route route;
    private int position;

    private final int PERMISSION_KAY = 155;
    private List<String> permissionList;

    private BroadcastReceiver receiver;

    private Fragment fragment;

    private FileHelper fileHelper;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_route);

        ButterKnife.bind(this);

        route = (Route)getIntent().getSerializableExtra("route");
        position = getIntent().getIntExtra("position", 0);

        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);

        actionBar.setTitle(route.getName());

        fileHelper = new FileHelper(this);


        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                invalidateOptionsMenu();
            }
        };

        registerReceiver(receiver, new IntentFilter("app.send.file"));

        /*Intent intent = new Intent(this, BackgroundVideoRecorder.class);
        intent.putExtra("route_id", route.getId());

        if (Utils.isOreo()) {
            startForegroundService(intent);
        }else {
            startService(intent);
        }*/

    }

    public int getToolbarHeight(){
        return toolbar.getMeasuredHeight();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (fragment == null){
            if (checkPermission()){
                enableLocation();
            }
        }

    }

    public boolean checkPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!Settings.canDrawOverlays(this)) {
                Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:" + getPackageName()));
                startActivity(intent);

                return false;
            }
        }

        return true;
    }

    private boolean enableLocation(){
        permissionList = new ArrayList<>();

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
            permissionList.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
            permissionList.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)
            permissionList.add(Manifest.permission.ACCESS_FINE_LOCATION);
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
            permissionList.add(Manifest.permission.ACCESS_COARSE_LOCATION);
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)
            permissionList.add(Manifest.permission.CAMERA);
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED)
            permissionList.add(Manifest.permission.RECORD_AUDIO);


        int size = permissionList.size();
        String[] permissions = new String[size];
        for (int i = 0; i < permissionList.size(); i++) {
            permissions[i] = permissionList.get(i);
        }

        if (size > 0) {
            ActivityCompat.requestPermissions(this, permissions, PERMISSION_KAY);
            return false;
        }

        fragment = new RouteTabsFragment();

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.main_frame, fragment)
                .commitAllowingStateLoss();


        return true;

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if (requestCode == PERMISSION_KAY) {
            boolean ACCESS_FINE_LOCATION = false;
            boolean ACCESS_COARSE_LOCATION = false;
            boolean CAMERA = false;
            boolean AUDIO = false;
            boolean READ_EXTERNAL_STORAGE = false;
            boolean WRITE_EXTERNAL_STORAGE = false;
            boolean SYSTEM_ALERT_WINDOW = false;


            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED)
                ACCESS_FINE_LOCATION = true;
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED)
                ACCESS_COARSE_LOCATION = true;
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED)
                CAMERA = true;
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED)
                AUDIO = true;
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
                READ_EXTERNAL_STORAGE = true;
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
                WRITE_EXTERNAL_STORAGE = true;
            if (ACCESS_COARSE_LOCATION && ACCESS_FINE_LOCATION && CAMERA && AUDIO && READ_EXTERNAL_STORAGE && WRITE_EXTERNAL_STORAGE && SYSTEM_ALERT_WINDOW) {
                enableLocation();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

    }

    public List<House> getHouses(){
        if (route.getHouses() == null){
            return new ArrayList<House>();
        }
        return route.getHouses();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        menu.removeItem(20);

        File folder = Utils.getBaseFolder(this, route.getId());

        File[] list = folder.listFiles();

        if (route.getStatusRoute() == 4 && !App.getInstance().isSend() && list != null && list.length > 0){
            menu.add(0, 20, 0, "Отправить отчет").setIcon(R.drawable.ic_baseline_email_24px).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                return true;
            case 20:
                new AlertDialog.Builder(this)
                        .setTitle("Отправить отчет")
                        .setMessage("Нажмите Ок для отправки отчета")
                        .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                File folder = Utils.getBaseFolder(RouteActivity.this, route.getId());
                                File[] list = folder.listFiles();
                                if (list != null && list.length > 0){
                                    fileHelper.startAction(route.getId(), App.getInstance().getUserLogin().getToken());
                                }else {
                                    Toast.makeText(RouteActivity.this, "Нет файлов для отправки", Toast.LENGTH_LONG).show();
                                }

                                invalidateOptionsMenu();
                            }
                        })
                        .setNegativeButton("Отмена", null)
                        .show();


                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (route.getStatusRoute() == 6){
            setRouteStatus(5, new StatusChangeListener() {
                @Override
                public void onSuccess(int status) {
                    RouteActivity.super.onBackPressed();
                }
            });
        }else {
            super.onBackPressed();
        }
    }

    public void setRouteStatus(final int status, final StatusChangeListener listener){

        final ProgressDialog dialog = new ProgressDialog(this);
        dialog.setCancelable(false);
        dialog.setMessage("Смена статуса...");
        dialog.show();

        Call<PostResult> call = App.getRetrofitApi().setRouteStatus(App.getInstance().getUserLogin().getToken(), route.getId(), status);
        call.enqueue(new Callback<PostResult>() {
            @Override
            public void onResponse(Call<PostResult> call, Response<PostResult> response) {
                dialog.dismiss();

                route.setStatusRoute(status);

                listener.onSuccess(status);

                invalidateOptionsMenu();
            }

            @Override
            public void onFailure(Call<PostResult> call, Throwable t) {
                dialog.dismiss();
            }
        });
    }

    public interface StatusChangeListener{
        void onSuccess(int status);
    }

    @Override
    protected void onDestroy() {

        unregisterReceiver(receiver);
        Intent intent = new Intent();

        intent.putExtra("route", route);
        intent.putExtra("position", position);

        setResult(RESULT_OK, intent);
        super.onDestroy();
    }
}
