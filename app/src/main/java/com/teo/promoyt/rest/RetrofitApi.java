package com.teo.promoyt.rest;


import com.teo.promoyt.rest.get.GetLogin;
import com.teo.promoyt.rest.get.GetRoutes;
import com.teo.promoyt.rest.get.GetStatus;
import com.teo.promoyt.rest.get.GetTz;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface RetrofitApi {

    @GET("login")
    Call<GetLogin> login(@Query("login") String login, @Query("password") String password, @Query("phone_id") String phone_id, @Query("device_token") String device_token);

    @GET("get-promoyt-status")
    Call<GetStatus> getStatus(@Query("token") String token);

    @GET("get-routes")
    Call<GetRoutes> getRoutes(@Query("token") String token);

    @FormUrlEncoded
    @POST("apply-route")
    Call<PostResult> applyPoutes(@Field("token") String token,@Field("route") String route);

    @FormUrlEncoded
    @POST("cancel-route")
    Call<PostResult> cancelPoutes(@Field("token") String token,@Field("route") String route, @Field("comment") String comment);

    @GET("set-address-data")
    Call<PostResult> setAddress(@Query("data") String token);

    @GET("get-route-tz")
    Call<GetTz> getTz(@Query("token") String token, @Query("route") String route);

    @FormUrlEncoded
    @POST("set-geo")
    Call<PostResult> setGeo(@Field("timecode") int timecode, @Field("token") String token,@Field("route_id") String route_id, @Field("coord_x") Double coord_x, @Field("coord_y") Double coord_y);

    @FormUrlEncoded
    @POST("set-route-status")
    Call<PostResult> setRouteStatus(@Field("token") String token,@Field("route") String route, @Field("status") int status);

    @Multipart
    @POST("upload-video")
    Call<PostResult> upload(@Part("token") String token,@Part("date") String date, @Part("route") String route, @Part("MAX_FILE_SIZE") long file_size, @Part MultipartBody.Part file);

    @Multipart
    @POST("upload-video")
    Call<PostResult> upload(@Part("token") String token,@Part("date") String date, @Part("route") String route, @Part("MAX_FILE_SIZE") long file_size, @Part MultipartBody.Part file, @Part("uploads_files") int uploads_files);

}
