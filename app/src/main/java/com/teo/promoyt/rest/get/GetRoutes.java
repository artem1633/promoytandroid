package com.teo.promoyt.rest.get;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.teo.promoyt.model.Route;

import java.util.List;

public class GetRoutes {

    @SerializedName("errors")
    @Expose
    private String errors;
    @SerializedName("data")
    @Expose
    private List<Route> data = null;

    public String getErrors() {
        return errors;
    }

    public void setErrors(String errors) {
        this.errors = errors;
    }

    public List<Route> getData() {
        return data;
    }

    public void setData(List<Route> data) {
        this.data = data;
    }
}
