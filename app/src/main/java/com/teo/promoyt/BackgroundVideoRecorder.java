package com.teo.promoyt;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.hardware.Camera;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;
import android.support.v4.app.NotificationManagerCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.teo.promoyt.classes.Utils;

public class BackgroundVideoRecorder extends Service {

    private WindowManager windowManager;
    private NotificationManagerCompat mNotificationManager;
    public static final String CHANNEL_ID = "trablone_channel_01";
    private CameraHelper cameraHelper;
    private long mNotificationPostTime = 0;
    private View currentView;

    public void setHeight(int height) {
        this.height = height;
    }

    private int height;

    @Override
    public void onCreate() {

        // Create new SurfaceView, set its size to 1x1, move it to the top left corner and set this service as a callback
        windowManager = (WindowManager) this.getSystemService(Context.WINDOW_SERVICE);

        mNotificationManager = NotificationManagerCompat.from(this);
        createNotificationChannel();


    }

    private void createNotificationChannel() {
        if (Utils.isOreo()) {
            CharSequence name = "TMP";
            int importance = NotificationManager.IMPORTANCE_LOW;
            NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
            manager.createNotificationChannel(mChannel);
        }
    }

    private Notification buildNotification() {

        Intent nowPlayingIntent = new Intent("action.record.video");
        PendingIntent clickIntent = PendingIntent.getActivity(this, 0, nowPlayingIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        if (mNotificationPostTime == 0) {
            mNotificationPostTime = System.currentTimeMillis();
        }

        android.support.v4.app.NotificationCompat.Builder builder = new android.support.v4.app.NotificationCompat.Builder(this, CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_menu_camera)
                .setContentIntent(clickIntent)
                .setContentTitle("Запись видео")
                .setWhen(mNotificationPostTime);

        if (Utils.isJellyBeanMR1()) {
            builder.setShowWhen(false);
        }

        if (Utils.isOreo()) {
            builder.setColorized(true);
        }

        Notification n = builder.build();


        return n;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {


        return super.onStartCommand(intent, flags, startId);
    }


    public void start(String id) {
        Log.e("tr", "start " + id);
        cameraHelper = new CameraHelper(this, id);
        Notification notification = buildNotification();
        if (notification != null) {

            if (Utils.isOreo()){
                startForeground(1002, notification);
            }else {
                mNotificationManager.notify(1002, notification);
            }
        }
        cameraHelper.start(new CameraHelper.OnStartListener() {
            @Override
            public void onStart(Camera.Size size, SurfaceView surfaceView) {
                int layout_parms;

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    layout_parms = WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY;
                } else {
                    layout_parms = WindowManager.LayoutParams.TYPE_PHONE;
                }

                final WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams(
                        height, height, layout_parms,
                        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE | WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN | WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                        PixelFormat.TRANSLUCENT
                );

                layoutParams.gravity = Gravity.RIGHT | Gravity.TOP;

                currentView = surfaceView;

                windowManager.addView(currentView, layoutParams);
            }
        });

    }

    @Override
    public void onDestroy() {
        Log.e("tr", "onDestroy " + this);
        if (Utils.isOreo()){
            stopForeground(true);
        }else {
            mNotificationManager.cancel(1002);
        }
    }

    public void stop() {

        Log.e("tr", "stop " + this);
        try {

            if (cameraHelper != null) {
                cameraHelper.stop();
            }


            if (currentView != null) {
                windowManager.removeView(currentView);
            }


        } catch (Throwable e) {
            e.printStackTrace();
        }

        if (Utils.isOreo()){
            stopForeground(true);
        }else {
            mNotificationManager.cancel(1002);
        }
    }

    MyBinder binder = new MyBinder();

    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    public class MyBinder extends Binder {
        public BackgroundVideoRecorder getService() {
            return BackgroundVideoRecorder.this;
        }
    }
}
