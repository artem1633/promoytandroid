package com.teo.promoyt;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.PowerManager;
import android.util.Log;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.MenuItem;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;

import android.support.v7.widget.Toolbar;

import com.teo.promoyt.activityes.BaseActivity;
import com.teo.promoyt.activityes.LoginActivity;
import com.teo.promoyt.database.UserLogin;
import com.teo.promoyt.fragments.route.RoutesRouteFragment;
import com.teo.promoyt.rest.get.GetRoutes;
import com.teo.promoyt.rest.get.GetStatus;
import com.teo.promoyt.fragments.BaseFragment;
import com.teo.promoyt.fragments.income.MyIncomeFragment;
import com.teo.promoyt.fragments.news.NewsFragment;
import com.teo.promoyt.fragments.route.routes.RouteTabsFragment;
import com.teo.promoyt.fragments.status.MyStatusFragment;
import android.os.PowerManager.WakeLock;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.os.PowerManager.PARTIAL_WAKE_LOCK;

public class MainActivity extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    @BindView(R.id.nav_view)
    NavigationView navigationView;
    BaseFragment fragment;

    private MenuItem selectItem;

    private WakeLock mWakeLock;

    @SuppressLint({"InvalidWakeLockTag", "WakelockTimeout"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        this.mWakeLock = ((PowerManager) getSystemService(POWER_SERVICE)).newWakeLock(PARTIAL_WAKE_LOCK, "PromoutLock");
        this.mWakeLock.acquire();

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(this);


        if (App.getInstance().getUserLogin() != null){

            Call<GetStatus> call = App.getRetrofitApi().getStatus(App.getInstance().getUserLogin().getToken());
            call.enqueue(new Callback<GetStatus>() {
                @Override
                public void onResponse(Call<GetStatus> call, Response<GetStatus> response) {
                    if (response != null && response.body() != null){
                        GetStatus getStatus = response.body();
                        if (getStatus.getStatus() > 0){
                            //finish();
                        }else {
                            onNavigationItemSelected(navigationView.getMenu().findItem(R.id.nav_route));
                        }
                    }

                }

                @Override
                public void onFailure(Call<GetStatus> call, Throwable t) {

                }
            });
        }else {
            startActivity(new Intent(this, LoginActivity.class));
            finish();
        }

    }


    @Override
    public void onBackPressed() {

        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        if (selectItem != null){
            selectItem.setChecked(false);
        }

        selectItem = item;

        selectItem.setChecked(true);


        switch (item.getItemId()){
            case R.id.nav_income:
                fragment = new MyIncomeFragment();
                break;
            case R.id.nav_status:
                fragment = new MyStatusFragment();
                break;
            case R.id.nav_route:
                fragment = new RoutesRouteFragment();
                break;
            case R.id.nav_news:
                fragment = new NewsFragment();
                break;
            case R.id.nav_exit:
                App.getInstance().getDatabase().getHistoryDao().delete(App.getInstance().getUserLogin());
                startActivity(new Intent(this, LoginActivity.class));
                finish();
                break;
        }

        if (fragment != null){
            handler.post(new Runnable() {
                @Override
                public void run() {
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.main_frame, fragment)
                            .commitAllowingStateLoss();
                }
            });
        }

        drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onDestroy() {
        this.mWakeLock.release();
        super.onDestroy();
    }
}
