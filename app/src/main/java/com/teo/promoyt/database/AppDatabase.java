package com.teo.promoyt.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;


@Database(entities = {UserLogin.class, UserLocation.class}, version = 2)
public abstract class AppDatabase extends RoomDatabase {

    public abstract UserLoginDao getHistoryDao();
    public abstract LocationDao getLocationDao();



}
