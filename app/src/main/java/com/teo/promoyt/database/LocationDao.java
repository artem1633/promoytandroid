package com.teo.promoyt.database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public abstract class LocationDao {
    @Query("SELECT * FROM location ORDER BY date ASC")
    public abstract List<UserLocation> getAll();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void insert(UserLocation... wallpapers);

    @Delete
    public abstract void delete(UserLocation... wallpapers);

    @Update
    public abstract void update(UserLocation... wallpapers);
}
