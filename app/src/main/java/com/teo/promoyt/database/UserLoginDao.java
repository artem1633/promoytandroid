package com.teo.promoyt.database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;


import java.util.List;


@Dao
public abstract class UserLoginDao {
    @Query("SELECT * FROM user_login")
    public abstract List<UserLogin> getAll();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void insert(UserLogin... wallpapers);

    @Delete
    public abstract void delete(UserLogin... wallpapers);

    @Update
    public abstract void update(UserLogin... wallpapers);
}
