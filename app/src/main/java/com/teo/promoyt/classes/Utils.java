package com.teo.promoyt.classes;

import android.content.Context;
import android.content.Intent;
import android.media.effect.Effect;
import android.os.Build;
import android.os.Environment;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Utils {

    public static void showActivity(Context context, Class activity){

        Intent intent = new Intent(context, activity);
        context.startActivity(intent);
    }


    public static File getBaseFolder(Context context, String name){
        File file = new File(Environment.getExternalStorageDirectory(), "Promout");//context.getFilesDir();//
        if (!file.exists()){
            file.mkdirs();
        }

        File folder = new File(file, name);
        if (!folder.exists()){
            folder.mkdirs();
        }

        return folder;
    }

    public static File getCamFile(File folder){
        Date date = new Date(System.currentTimeMillis());
        String name = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date);
        File[] list = folder.listFiles();

        File file = new File(folder, name + " part_" + list.length + ".mp4");

        return file;
    }

    public static boolean isOreo() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.O;
    }

    public static boolean isMarshmallow() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.M;
    }

    public static boolean isLollipop() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP;
    }

    public static boolean isJellyBeanMR2() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2;
    }

    public static boolean isJellyBean() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN;
    }

    public static boolean isJellyBeanMR1() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1;
    }
}
