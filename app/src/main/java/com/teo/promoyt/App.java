package com.teo.promoyt;

import android.arch.persistence.room.Room;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.preference.PreferenceManager;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;

import com.google.firebase.FirebaseApp;
import com.google.firebase.messaging.FirebaseMessaging;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.teo.promoyt.database.AppDatabase;
import com.teo.promoyt.database.UserLogin;
import com.teo.promoyt.rest.RetrofitApi;
import com.yandex.metrica.YandexMetrica;
import com.yandex.metrica.YandexMetricaConfig;


import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class App extends MultiDexApplication {

    private static Cache mCache;
    private static OkHttpClient mOkHttpClient;
    private static Retrofit mRetrofit;
    private static RetrofitApi mRetrofitApi;

    private AppDatabase database;

    public boolean isSend() {
        return send;
    }

    public UserLogin getUserLogin() {
        List<UserLogin> userLoginList = App.getInstance().getDatabase().getHistoryDao().getAll();
        if (userLoginList.size() > 0) {
            setUserLogin(userLoginList.get(0));
        }
        return userLogin;
    }

    public void setUserLogin(UserLogin userLogin) {
        this.userLogin = userLogin;
    }

    private UserLogin userLogin;

    public void setSend(boolean send) {
        this.send = send;
        Intent intent1 = new Intent("app.send.file");
        sendBroadcast(intent1);
    }

    public boolean send = false;

    private static App mInstance;

    public SharedPreferences getPreferences() {
        return preferences;
    }

    private SharedPreferences preferences;

    public static synchronized App getInstance() {
        return mInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        MultiDex.install(this);
        mInstance = this;

        FirebaseApp.initializeApp(this);
        FirebaseMessaging.getInstance().setAutoInitEnabled(true);

        preferences = PreferenceManager.getDefaultSharedPreferences(this);

        initImageLoader(this);

        YandexMetricaConfig config = YandexMetricaConfig.newConfigBuilder("26d3f741-7011-4463-b9ec-762deb3c1ca8").build();
        YandexMetrica.activate(getApplicationContext(), config);
        YandexMetrica.enableActivityAutoTracking(this);

        database = Room.databaseBuilder(this, AppDatabase.class, "database")
                .allowMainThreadQueries()
                .fallbackToDestructiveMigration()
                .build();

        provideHttpCache();
        provideOkhttpClient();
        provideRetrofit();
        provideRetrofitApi();

    }

    public AppDatabase getDatabase() {
        return database;
    }

    public static void initImageLoader(Context context) {
        DisplayImageOptions options = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .showStubImage(R.mipmap.ic_launcher)
                .resetViewBeforeLoading()
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
                .threadPriority(Thread.NORM_PRIORITY - 2)
                .denyCacheImageMultipleSizesInMemory()
                .diskCacheFileNameGenerator(new Md5FileNameGenerator())
                .diskCacheSize(50 * 1024 * 1024) // 50 Mb
                .tasksProcessingOrder(QueueProcessingType.LIFO)
                .defaultDisplayImageOptions(options)
                .build();

        ImageLoader.getInstance().init(config);
    }

    private void provideHttpCache() {
        int cacheSize = 10 * 1024 * 1024;
        mCache = new Cache(getApplicationContext().getCacheDir(), cacheSize);
    }

    private void provideOkhttpClient() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder client = new OkHttpClient.Builder();
        client.cache(mCache);
        client.readTimeout(9, TimeUnit.HOURS);
        client.connectTimeout(9, TimeUnit.HOURS);
        client.writeTimeout(9, TimeUnit.HOURS);
        client.callTimeout(9, TimeUnit.HOURS);
        client.addInterceptor(interceptor);
        client.followRedirects(true);
        mOkHttpClient = client.build();
    }

    private void provideRetrofit() {
        mRetrofit = new Retrofit.Builder()
                .baseUrl("http://promoyt.teo-crm.com/api/v1/")
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(mOkHttpClient)
                .build();
    }

    private static void provideRetrofitApi() {
        mRetrofitApi = mRetrofit.create(RetrofitApi.class);
    }

    public static RetrofitApi getRetrofitApi() {
        return mRetrofitApi;
    }

}
