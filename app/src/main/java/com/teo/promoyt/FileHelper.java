package com.teo.promoyt;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;

import com.teo.promoyt.classes.Utils;
import com.teo.promoyt.rest.CountingRequestBody;
import com.teo.promoyt.rest.PostResult;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FileHelper {

    public FileHelper(Context context) {
        this.context = context;

        routes = new ArrayList<>();
        mNotificationManager = NotificationManagerCompat.from(context);
        createNotificationChannel();
        dialog = new ProgressDialog(context);
        dialog.setCancelable(false);

    }

    private Context context;
    private ProgressDialog dialog;

    public void startAction(String route_id, String token) {
        if (!routes.contains(route_id)){
            App.getInstance().setSend(true);
            routes.add(route_id);
            dialog.show();
            startUpload(token, route_id);
        }
    }

    public static final String CHANNEL_ID = "promout_file_sender";
    private long mNotificationPostTime = 0;

    private List<String> routes;

    private NotificationManagerCompat mNotificationManager;


    private void createNotificationChannel() {

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            CharSequence name = "Promout";
            int importance = NotificationManager.IMPORTANCE_LOW;
            NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
            manager.createNotificationChannel(mChannel);
        }
    }


    private void startUpload(String token, String route){
        File folder = Utils.getBaseFolder(context, route);
        File[] list = folder.listFiles();
        uploadFile(list, 0, token, route);
    }


    private boolean send = false;
    private int failcount;

    public void uploadFile(final File[] files, final int position, final String token, final String route){


        try {
            if (files != null && files.length > 0){

                send = true;
                final File file = files[position];
                final NotificationCompat.Builder builder = buildNotification();
                final int filePosition = position + 1;
                updateNotif(builder, "Отправка" + filePosition + "/" + files.length, Integer.parseInt(route));

                final RequestBody requestFile = RequestBody.create(MediaType.parse("video/*"), file);

                // MultipartBody.Part is used to send also the actual file name
                String name = file.getName();
                String date = name.substring(0, name.indexOf(" part_"));
                Log.e("tr", "name " + name + " date " + date);
                RequestBody requestBody = new CountingRequestBody(requestFile, new CountingRequestBody.Listener() {
                    @Override
                    public void onRequestProgress(final long bytesWritten, final long contentLength) {
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                //Log.e("tr", "bw " + bytesWritten + " cl " + contentLength);
                                builder.setProgress((int)contentLength, (int)bytesWritten, false);
                                updateNotif(builder,"Отправка" + filePosition + "/" + files.length, Integer.parseInt(route));
                                dialog.setMax((int)contentLength);
                                dialog.setProgress((int)bytesWritten);
                            }
                        });


                    }
                });

                MultipartBody.Part body = MultipartBody.Part.createFormData("file", name, requestBody);

                int uploads_files = position == files.length - 1 ? 1 : 0;

                Call<PostResult> call;
                if (uploads_files == 1) {
                    call = App.getRetrofitApi().upload(token, date, route, file.length(), body, uploads_files);
                }else {
                    call = App.getRetrofitApi().upload(token, date, route, file.length(), body);
                }

                call.enqueue(new Callback<PostResult>() {
                    @Override
                    public void onResponse(Call<PostResult> call, Response<PostResult> response) {

                        if (response.body() == null){
                            builder.setProgress(0, 0, false);
                            updateNotif(builder, "Ошибка отправки, проверьте соединение!",  Integer.parseInt(route));
                            dialog.dismiss();
                            App.getInstance().setSend(false);
                        }else if (response.body().getErrors() == null){
                            Log.e("tr", "position " + position);

                            if (file.delete()){
                                if (position < files.length - 1){
                                    uploadFile(files, filePosition, token, route);

                                }else {
                                    builder.setProgress(0, 0, false);
                                    updateNotif(builder, "Отправка завершена", Integer.parseInt(route));
                                    send = false;
                                    dialog.dismiss();
                                    App.getInstance().setSend(false);
                                }
                            }

                        }else {
                            Log.e("tr", "Error " + response.body().getErrors());
                            routes.remove(route);
                            mNotificationManager.cancel(Integer.parseInt(route));

                            final NotificationCompat.Builder builder = buildNotification();
                            builder.setProgress(0, 0, false);
                            updateNotif(builder, response.body().getErrors() + " - "+ file.getName(), -1);
                            dialog.dismiss();
                            App.getInstance().setSend(false);
                        }
                    }

                    @Override
                    public void onFailure(Call<PostResult> call, Throwable t) {
                        t.printStackTrace();
                        Log.e("tr", "failure " + t.getMessage());
                        mNotificationManager.cancel(Integer.parseInt(route));
                        failcount++;

                        if (failcount < 3 && position < files.length - 1){
                            uploadFile(files, position, token, route);
                        }else {
                            final NotificationCompat.Builder builder = buildNotification();
                            builder.setProgress(0, 0, false);
                            updateNotif(builder, "Ошибка отправки " + position + "/" + files.length + " " +t.getMessage() + " - "+ file.getName(), -1);
                            dialog.dismiss();
                            App.getInstance().setSend(false);
                        }
                    }

                });
            }

        }catch (Throwable e){
            e.printStackTrace();
        }
    }

    private void updateNotif(NotificationCompat.Builder builder, String text, int id){
        builder.setContentText(text);

        mNotificationManager.notify(id, builder.build());

        dialog.setMessage(text);

    }

    private NotificationCompat.Builder buildNotification() {

        if (mNotificationPostTime == 0) {
            mNotificationPostTime = System.currentTimeMillis();
        }

        android.support.v4.app.NotificationCompat.Builder builder = new NotificationCompat.Builder(context, CHANNEL_ID)
                .setSmallIcon(Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP ? R.drawable.baseline_email_white_24 : R.drawable.ic_baseline_email_24px)

                .setContentTitle("Отправка файлов")
                .setProgress(100, 0, false)
                .setWhen(mNotificationPostTime);

        if (Utils.isJellyBeanMR1()) {
            builder.setShowWhen(false);
        }

        if (Utils.isOreo()) {
            builder.setColorized(true);
        }

        return builder;
    }


    public void onDestroy() {
        App.getInstance().setSend(false);
        //mNotificationManager.cancelAll();
        if (send){
            final NotificationCompat.Builder builder = buildNotification();
            builder.setProgress(0, 0, false);
            updateNotif(builder, "Сервис отправки остановлен системой", 10);
        }

    }

}
