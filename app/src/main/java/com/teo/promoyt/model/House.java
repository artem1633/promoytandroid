package com.teo.promoyt.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class House implements Serializable {

    @SerializedName("id")
    @Expose
    private String id;

    public int getStatus_api() {
        return status_api;
    }

    public void setStatus_api(int status_api) {
        this.status_api = status_api;
    }

    @SerializedName("status_api")
    @Expose
    private int status_api;

    @SerializedName("country")
    @Expose
    private Object country;

    @SerializedName("town")
    @Expose
    private String town;

    @SerializedName("street")
    @Expose
    private String street;

    @SerializedName("house")
    @Expose
    private String house;

    @SerializedName("coord_x")
    @Expose
    private Double coordX;

    @SerializedName("coord_y")
    @Expose
    private Double coordY;

    @SerializedName("floor")
    @Expose
    private String floor;

    @SerializedName("apartament")
    @Expose
    private String apartament;

    @SerializedName("entrance")
    @Expose
    private int entrance;

    @SerializedName("type")
    @Expose
    private Object type;

    @SerializedName("region")
    @Expose
    private String region;

    @SerializedName("housing")
    @Expose
    private String housing;

    @SerializedName("porter")
    @Expose
    private String porter;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Object getCountry() {
        return country;
    }

    public void setCountry(Object country) {
        this.country = country;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getHouse() {
        return house;
    }

    public void setHouse(String house) {
        this.house = house;
    }

    public Double getCoordX() {
        return coordX;
    }

    public void setCoordX(Double coordX) {
        this.coordX = coordX;
    }

    public Double getCoordY() {
        return coordY;
    }

    public void setCoordY(Double coordY) {
        this.coordY = coordY;
    }

    public String getFloor() {
        return floor;
    }

    public void setFloor(String floor) {
        this.floor = floor;
    }

    public String getApartament() {
        return apartament;
    }

    public void setApartament(String apartament) {
        this.apartament = apartament;
    }

    public int getEntrance() {
        return entrance;
    }

    public void setEntrance(int entrance) {
        this.entrance = entrance;
    }

    public Object getType() {
        return type;
    }

    public void setType(Object type) {
        this.type = type;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getHousing() {
        return housing;
    }

    public void setHousing(String housing) {
        this.housing = housing;
    }

    public String getPorter() {
        return porter;
    }

    public void setPorter(String porter) {
        this.porter = porter;
    }
}
