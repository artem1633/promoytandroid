package com.teo.promoyt.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Route implements Serializable {

    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("status_route")
    @Expose
    private int statusRoute;

    @SerializedName("start_date")
    @Expose
    private String startDate;

    @SerializedName("fact_date")
    @Expose
    private String factDate;

    @SerializedName("comment_employees")
    @Expose
    private Object commentEmployees;

    @SerializedName("comment_manager")
    @Expose
    private String commentManager;

    @SerializedName("count_adress")
    @Expose
    private String countAdress;

    @SerializedName("fact_count_adress")
    @Expose
    private Object factCountAdress;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("normal")
    @Expose
    private Object normal;

    @SerializedName("completed_count")
    @Expose
    private Object completedCount;

    @SerializedName("fines_sum")
    @Expose
    private Object finesSum;

    @SerializedName("comment_leader")
    @Expose
    private Object commentLeader;

    @SerializedName("comment_quality_manager")
    @Expose
    private Object commentQualityManager;

    @SerializedName("town")
    @Expose
    private String town;

    @SerializedName("region")
    @Expose
    private String region;

    @SerializedName("plane_start_date")
    @Expose
    private String planeStartDate;

    @SerializedName("plane_end_date")
    @Expose
    private String planeEndDate;

    @SerializedName("maket")
    @Expose
    private String maket;

    @SerializedName("count_apartament")
    @Expose
    private String countApartament;

    @SerializedName("count_entrance")
    @Expose
    private String countEntrance;

    @SerializedName("fact")
    @Expose
    private Object fact;

    @SerializedName("fact_count_address")
    @Expose
    private Object factCountAddress;

    @SerializedName("category")
    @Expose
    private Object category;

    @SerializedName("project")
    @Expose
    private String project;

    @SerializedName("zone")
    @Expose
    private String zone;

    @SerializedName("placement")
    @Expose
    private String placement;

    public String getPayment() {
        return payment;
    }

    public void setPayment(String payment) {
        this.payment = payment;
    }

    @SerializedName("project_payment")
    @Expose
    private String payment;

    public String getCirculation() {
        return circulation;
    }

    public void setCirculation(String circulation) {
        this.circulation = circulation;
    }

    @SerializedName("project_circulation")
    @Expose
    private String circulation;

    @SerializedName("houses")
    @Expose
    private List<House> houses = null;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getStatusRoute() {
        return statusRoute;
    }

    public void setStatusRoute(int statusRoute) {
        this.statusRoute = statusRoute;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getFactDate() {
        return factDate;
    }

    public void setFactDate(String factDate) {
        this.factDate = factDate;
    }

    public Object getCommentEmployees() {
        return commentEmployees;
    }

    public void setCommentEmployees(Object commentEmployees) {
        this.commentEmployees = commentEmployees;
    }

    public String getCommentManager() {
        return commentManager;
    }

    public void setCommentManager(String commentManager) {
        this.commentManager = commentManager;
    }

    public String getCountAdress() {
        return countAdress;
    }

    public void setCountAdress(String countAdress) {
        this.countAdress = countAdress;
    }

    public Object getFactCountAdress() {
        return factCountAdress;
    }

    public void setFactCountAdress(Object factCountAdress) {
        this.factCountAdress = factCountAdress;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Object getNormal() {
        return normal;
    }

    public void setNormal(Object normal) {
        this.normal = normal;
    }

    public Object getCompletedCount() {
        return completedCount;
    }

    public void setCompletedCount(Object completedCount) {
        this.completedCount = completedCount;
    }

    public Object getFinesSum() {
        return finesSum;
    }

    public void setFinesSum(Object finesSum) {
        this.finesSum = finesSum;
    }

    public Object getCommentLeader() {
        return commentLeader;
    }

    public void setCommentLeader(Object commentLeader) {
        this.commentLeader = commentLeader;
    }

    public Object getCommentQualityManager() {
        return commentQualityManager;
    }

    public void setCommentQualityManager(Object commentQualityManager) {
        this.commentQualityManager = commentQualityManager;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getPlaneStartDate() {
        return planeStartDate;
    }

    public void setPlaneStartDate(String planeStartDate) {
        this.planeStartDate = planeStartDate;
    }

    public String getPlaneEndDate() {
        return planeEndDate;
    }

    public void setPlaneEndDate(String planeEndDate) {
        this.planeEndDate = planeEndDate;
    }

    public String getMaket() {
        return maket;
    }

    public void setMaket(String maket) {
        this.maket = maket;
    }

    public String getCountApartament() {
        return countApartament;
    }

    public void setCountApartament(String countApartament) {
        this.countApartament = countApartament;
    }

    public String getCountEntrance() {
        return countEntrance;
    }

    public void setCountEntrance(String countEntrance) {
        this.countEntrance = countEntrance;
    }

    public Object getFact() {
        return fact;
    }

    public void setFact(Object fact) {
        this.fact = fact;
    }

    public Object getFactCountAddress() {
        return factCountAddress;
    }

    public void setFactCountAddress(Object factCountAddress) {
        this.factCountAddress = factCountAddress;
    }

    public Object getCategory() {
        return category;
    }

    public void setCategory(Object category) {
        this.category = category;
    }

    public String getProject() {
        return project;
    }

    public void setProject(String project) {
        this.project = project;
    }

    public String getZone() {
        return zone;
    }

    public void setZone(String zone) {
        this.zone = zone;
    }

    public String getPlacement() {
        return placement;
    }

    public void setPlacement(String placement) {
        this.placement = placement;
    }

    public List<House> getHouses() {
        return houses;
    }

    public void setHouses(List<House> houses) {
        this.houses = houses;
    }
}
