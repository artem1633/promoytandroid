package com.teo.promoyt.model;

public class Entrance {


    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getFloor() {
        return floor;
    }

    public void setFloor(int floor) {
        this.floor = floor;
    }

    public int getApartment() {
        return apartment;
    }

    public void setApartment(int apartment) {
        this.apartment = apartment;
    }

    public boolean isPorter() {
        return porter;
    }

    public void setPorter(boolean porter) {
        this.porter = porter;
    }

    public int getPorch() {
        return porch;
    }

    public void setPorch(int porch) {
        this.porch = porch;
    }

    public int getN() {
        return n;
    }

    public void setN(int n) {
        this.n = n;
    }

    private int porch;
    private int n;
    private int number;
    private int floor;
    private int apartment;
    private boolean porter;

}
